##################################################################
# Data sources to get AWS Backend and remote state
##################################################################
provider "aws" {
  region = "eu-west-1"
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "cv-cloud-remote-state"
    key    = "terraform/vpc"
    region = "eu-west-2"
  }
}

terraform {
  backend "s3" {
    bucket = "cv-cloud-remote-state"
    key    = "terraform/ec2"
    region = "eu-west-2"
  }
}

##################################################################
# Data sources to get AWS keys
##################################################################

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1ddz83D7DmbklDSh5jolnqnMy4krBNJr2akV3oW2CBeRaIuWssgESLYKGRqbqP6kflNwbPw/AYzAPGEXl+kK+ED7tVYOPfH8Sg4kXU48yQDTqdq3v2tOB7ahD6zr1HBHg1eJ6ZQh6qQ2ci2/V9+eUjiSp2aFE68HkAoNXoogBmVcVaGI7smY1NizFPB1iVcdmK4qoBMN7Yifmcy6FO8IhLIix/EusZ9x21BNoXb1B9/g3lp4hO6vOQHBED15RCjLv1YWMlGqUqPhquBYHXo5ge3d4/CGUsq82fP2B2gZ0J257gy/BJFqf8hX32vaO9r8Yo48gsN0Xq//rhfxc3th7 bjibodu@crowdvision.co.uk"
}

##################################################################
# Data sources to get VPC, subnet, security group and AMI details
##################################################################


resource "aws_security_group" "allow_ssh_bastion" {
  name        = "allow_ssh_bastion"
  description = "Allow ssh inbound traffic on port 22"
  vpc_id      = "${data.terraform_remote_state.network.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow_ssh_bastion"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "bastion" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  subnet_id              = "${element(data.terraform_remote_state.network.public_subnets, 2)}"
  key_name               = "${aws_key_pair.deployer.id}"
  vpc_security_group_ids = ["${aws_security_group.allow_ssh_bastion.id}", "${aws_security_group.all_local_comms.id}"]

  tags {
    Name = "bastion"
  }
}

resource "aws_eip" "bastion_public" {
  vpc      = true
  instance = "${aws_instance.bastion.id}"
}

######################################################################################
####configuring internal network ...
####################################################################################
  resource "aws_security_group" "all_local_comms" {
  name        = "all_local_comms"
  description = "Allow traffic local traffic"
  vpc_id      = "${data.terraform_remote_state.network.vpc_id}"
####################
  ingress {
     from_port       = 2049
     to_port         = 2049
     protocol        = "tcp"
     security_groups = ["${aws_security_group.allow_ssh_bastion.id}"]
   },
   tags {
     Name = "all_local_comm_nfs"
   }

############################
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = ["${aws_security_group.allow_ssh_bastion.id}"]
  }

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "all_local_comm"
  }
}
################################################################
### Data source for the template and user data setting installation
##############################################################
#
# data "template_file" "user-init" {
#       template = "${file("${path.module}/userdata.tpl")}"
# }
#
# data "template_file" "user-init3" {
#       template = "${file("${path.module}/fix_hosts.sh")}"
# }
# # user_data              =  "{data.template.user-init.rendered}"
# # user_data =  "${file("${path.module}/fix_hosts.sh")}"
# # user_data =  "${file("${path.module}/ppm.sh")}"
#
# data "template_file" "user_data" {
#   template = "${file("${path.module}/userdata.sh")}"

#########################################################
##launch configuration
#########################################################

resource "aws_launch_configuration" "as_conf" {
  # name_prefix   = "terraform-lc-example-"
  image_id      = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  # user_data = "{template_file.user-init3.rendered}"
  user_data =  "${file("${path.module}/scripts/fix_hosts.sh")}"

  lifecycle {
    create_before_destroy = true
  }
}

#######################################################################
###creation of the servers
#####################################################################
resource "aws_instance" "pnode1" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  subnet_id              = "${element(data.terraform_remote_state.network.private_subnets, 2)}"
  key_name               = "${aws_key_pair.deployer.id}"
  vpc_security_group_ids = ["${aws_security_group.allow_ssh_bastion.id}", "${aws_security_group.all_local_comms.id}"]
  private_ip             =  "10.22.204.72"
  user_data =  "${file("${path.module}/scripts/pnode.sh")}"

  # user_data              =  "{data.template_file.user-init.rendered}"
  tags {
    Name = "pnode1"
  }
}

resource "aws_instance" "vde" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  subnet_id              = "${element(data.terraform_remote_state.network.private_subnets, 2)}"
  key_name               = "${aws_key_pair.deployer.id}"
  vpc_security_group_ids = ["${aws_security_group.allow_ssh_bastion.id}", "${aws_security_group.all_local_comms.id}"]
  private_ip             =  "10.22.204.91"
  user_data =  "${file("${path.module}/scripts/vde.sh")}"
  tags {
    Name = "vde"
  }
}

resource "aws_instance" "ppml" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  subnet_id              = "${element(data.terraform_remote_state.network.private_subnets, 2)}"
  key_name               = "${aws_key_pair.deployer.id}"
  vpc_security_group_ids = ["${aws_security_group.allow_ssh_bastion.id}", "${aws_security_group.all_local_comms.id}"]
  private_ip             =  "10.22.204.95"
  user_data =  "${file("${path.module}/scripts/ppml.sh")}"
    # user_data              =  "{data.template_file.user-init2.userdata.sh.rendered}"
  tags {
    Name = "ppml"
  }
 }

resource "aws_instance" "ppm" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  subnet_id              = "${element(data.terraform_remote_state.network.private_subnets, 2)}"
  key_name               = "${aws_key_pair.deployer.id}"
  vpc_security_group_ids = ["${aws_security_group.allow_ssh_bastion.id}", "${aws_security_group.all_local_comms.id}"]
    private_ip             =   "10.22.204.96"
    user_data =  "${file("${path.module}/scripts/ppm.sh")}"
  tags {
    Name = "ppm"
  }
}

# resource "aws_instance" "rpndash" {
#   ami                    = "${data.aws_ami.ubuntu.id}"
#   instance_type          = "t2.micro"
#   subnet_id              = "${element(data.terraform_remote_state.network.private_subnets, 2)}"
#   key_name               = "${aws_key_pair.deployer.id}"
#   vpc_security_group_ids = ["${aws_security_group.allow_ssh_bastion.id}", "${aws_security_group.all_local_comms.id}"]
#     private_ip             = "10.22.204.5"
     # user_data =  "${file("${path.module}/scripts/rpndash.sh")}"
#

#   tags {
#     Name = "rpndash"
#   }
# }
